section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .return
        inc rax
        jmp .loop
    .return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    push r13
    push r14
    sub rsp, 20
    mov r12, rsp
    xor r13, r13
    mov rax, rdi
    mov r8, 10
    
    .loop:
        xor rdx, rdx
        div r8
        add dl, 0x30
        lea r14, [r12 + r13]
        mov byte[r14], dl
        inc r13
        cmp rax, 0
        je .print
        jmp .loop

    .print:
    	dec r13
        cmp r13, 0
        jl .stop
        xor rdi, rdi
        lea r14, [r12 + r13]
        mov dil, byte[r14]
        call print_char
        jmp .print
       
    .stop:
        add rsp, 20
        pop r14
        pop r13
        pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print:
        call print_uint     
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx				; текущий индекс символа
    xor rax, rax
    
    .loop:
        mov al, byte[rdi + rcx]
        cmp al, byte[rsi + rcx]      		; проверяем равны ли символы,
        jne .ret_false				; если нет возвращаем false
        cmp al, 0				; проверяем является ли символ конечным в обеих 
        je .ret_true				; строках, если да - то возвращаем true
        inc rcx				; иначе продолжаем сравнение
        jmp .loop

    .ret_false:
        mov rax, 0
        ret

    .ret_true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    xor rax, rax
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi	; адрес начала буфера
    mov r13, rsi	; размер буффера
    mov r14, 0		; позиция курсора
    sub r13, 1
    
    .pass_spaces:
        call read_char
        cmp rax, 0		; проверяем есть ли слово
        jz .err		; если нет возвращаем 0
        cmp rax, 0x20		; проверям символ на пробел
        je .pass_spaces	; если пробел пропускаем
        cmp rax, 0x9		; проверям символ на символ табуляции
        je .pass_spaces	; если символ табуляции пропускаем
        cmp rax, 0xA		; проверям символ на символ переноса строки
        je .pass_spaces	; если символ переноса пропускаем
    
    .fill_buf:
    	cmp r14, r13			; проверяем !(r14 < r13) т. е. !(текущая позиция < размер буфера)
    	jnb .err			; нельзя записать символ
    	mov byte [r12 + r14], al	; записываем символ в буфер
    	inc r14			; сдвигаем курсор
    	call read_char			; получаем след сивол
    	cmp rax, 0			; проверяем закончилось ли слово
    	jz .return			; если да делаем возврат значений
    	cmp rax, 0x20
        je .return
        cmp rax, 0x9
        je .return
        cmp rax, 0xA
        je .return
        jmp .fill_buf			; иначе продолжаем записывать буфер
        
    .return:
    	mov byte [r12 + r14], 0	; добавляем нуль-терминатор
    	mov rax, r12			
    	mov rdx, r14
    	jmp .final
        
    .err:
        mov rax, 0
        mov rdx, 0
    	jmp .final
    	
    .final:
        pop r14 ; возвращаем первоначальное состояние callee-saved регистров
        pop r13
        pop r12
        ret
    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx	; индекс символа
    xor r8, r8		; будет находится анализируемый символ
    
    .loop:
        mov r8b, byte[rdi + rcx]
        cmp r8, 0x30			; проверяем является ли символ -
        jb .stop			; символом цифры
        cmp r8, 0x39			; цифры в таблице ASCII с номерами 0x30 - 0x39
        ja .stop
        
        and r8b, 0xf			; превращаем символ в цифру
        mov r9, 10
        mul r9				; добавляем разряд 
        add rax, r8			; rax = rax * 10 + r8
        
        inc rcx			; переходим к следующему символу
    	jmp .loop
    	
    .stop:
    	mov rdx, rcx
    	
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8			; проверяем начинается ли строка с минуса
    mov r8b, byte[rdi]
    cmp r8, '-'
    je .parse_neg		; если число положительное используем функцию написанную ранее
    call parse_uint
    ret 
    
    .parse_neg:		; иначе сдвигаем указатель
    	add rdi, 1
    	call parse_uint
    	neg rax		; после инвертируем значение
    	add rdx, 1
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push r13
    push r14
    push r15
    mov r12, rdi		; указатель на строку
    mov r13, rsi		; указатель на буфер
    mov r14, rdx		; длина буфера
    call string_length		; получаем длинну строки
    add rax, 1
    mov r15, rax
    cmp r15, r14		; проверяем вмещается ли строка в буфер
    ja .err
    
    
    xor rcx, rcx
    .loop:
    	cmp rcx, r15
    	je .return
    	mov dil, byte[r12 + rcx]
    	mov byte[r13 + rcx], dil
    	inc rcx
    	jmp .loop
    
    .err:
    	xor rax, rax
    	jmp .finally
    	
    .return:
    	mov rax, r15
    	
    .finally:
    	pop r15
    	pop r14
    	pop r13
    	pop r12
    	ret
